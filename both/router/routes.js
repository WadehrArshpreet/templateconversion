Router.map(function () {
    this.route('home', {
        path: '/',
        template: 'home'
    });
    this.route('mailbox', {
        path: '/mail',
        template: 'mailbox'
    });
})